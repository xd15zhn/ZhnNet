import cv2 as cv
cap = cv.VideoCapture(0)
fourcc = cv.VideoWriter_fourcc(*'DIVX')
video = cv.VideoWriter('video1.avi', fourcc, 20, (640, 512), True)
cap.set(cv.CAP_PROP_FRAME_WIDTH, 1280)
cap.set(cv.CAP_PROP_FRAME_HEIGHT, 512)
save = False
cntframe = 200
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    img1 = frame[:, :640, :]
    img2 = frame[:, 640:, :]
    img1 = cv.copyMakeBorder(img1, 16, 16, 0, 0, cv.BORDER_CONSTANT, value=(255, 255, 255))
    img2 = cv.copyMakeBorder(img2, 16, 16, 0, 0, cv.BORDER_CONSTANT, value=(255, 255, 255))
    cv.imshow('test1', img1)
    cv.imshow('test2', img2)
    if save:
        video.write(img1)
        video.write(img2)
        cntframe -= 1
    c = cv.waitKey(100)
    if c == ord('q'):
        break
    elif c == ord('s'):
        save = True
    if cntframe <= 0:
        break
cap.release()
video.release()
