# ZhnNet

搭建目标检测网络,致力于用尽可能简单的网络和尽可能少的数据集检测你想识别的任何物体

## 文件结构
```
  ├── model: 搭建网络
  │     ├── backbone.py:特征提取主干网络
  │     ├── head.py:检测头和解码
  │     ├── zhnnet.py:网络
  ├── utils: 搭建训练网络时使用到的各种
  │     ├── dataset.py:加载数据集
  │     ├── loss.py:损失函数
  │     └── utils.py:其它函数
  ├── detect.py:检测
  ├── train.py:训练网络
  ├── camerashot.py:用摄像头拍摄视频数据集
  └── cameradetect.py:运行摄像头进行检测
```

可用CPU实现实时运行,但准确度很低,有待改进

详情及权重加QQ 1076214993  或QQ群 1026512757
